﻿using System;
using System.ComponentModel;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using TakeABreak.Tools;
using TakeABreak.ViewModels;
using TakeABreak.Views;

namespace TakeABreak
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private readonly MainViewModel viewModel;
        private Config config;
        private Timer timer;

        public MainWindow()
        {
            viewModel = new MainViewModel();

            InitializeComponent();
        }

        #region Private Methods
        /// <summary>
        /// Updates the application current status
        /// </summary>
        /// <param name="status"></param>
        private void UpdateStatus(string status)
        {
            viewModel.Status = $"Status: {status}";
        }

        /// <summary>
        /// Updates the information label
        /// </summary>
        /// <param name="info"></param>
        private void UpdateInformation(string info)
        {
            viewModel.Information = $"Information: {info}";
        }

        /// <summary>
        /// Starts the timer that counts the minutes for breaks
        /// </summary>
        private void Start()
        {
            int milliseconds = MillisecondsConverter.Convert(config.Minutes);
            timer = new Timer(milliseconds);
            timer.Elapsed += OnTimedEvent;
            timer.Enabled = true;

            viewModel.IsStarted = true;
            UpdateStatus("Running");
        }

        /// <summary>
        /// Stops the timer that counts the minutes for breaks
        /// </summary>
        private void Stop()
        {
            if (timer != null)
            {
                timer.Enabled = false;
            }

            viewModel.IsStarted = false;
            UpdateStatus("Stopped");
        }
        #endregion

        #region Events
        /// <summary>
        /// Start button event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            Start();
        }

        /// <summary>
        /// Stop button event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            Stop();
        }

        /// <summary>
        /// Settings button event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            SettingsWindow settingsWindow = new SettingsWindow();
            bool? result = settingsWindow.ShowDialog();

            if (result.HasValue && result.Value)
            {
                config = Config.Load();
            }
        }

        /// <summary>
        /// On timer timed event
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            timer.Stop();

            UpdateStatus("Paused");

            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                NotificationWindow notificationWindow = new NotificationWindow();
                bool? result = notificationWindow.ShowDialog();

                if (result.HasValue && result.Value)
                {
                    // The rest is taken
                    timer.Interval = MillisecondsConverter.Convert(config.Minutes);

                    if (config.CountBreaks)
                    {
                        viewModel.TotalBreaks += 1;
                        UpdateInformation($"Breaks taken today {viewModel.TotalBreaks}.");
                    }
                }
                else
                {
                    // 5 more minutes
                    timer.Interval = 300000;
                }
            });

            UpdateStatus("Running");
            timer.Start();
        }

        /// <summary>
        /// On application initialized event
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInitialized(EventArgs e)
        {
            Stop();

            config = Config.Load();
            if (config.StartMinimized)
            {
                Start();
                Hide();
            }

            UpdateInformation("N/A");

            if (config.CountBreaks)
            {
                UpdateInformation($"Breaks taken today {viewModel.TotalBreaks}.");
            }

            Grid.DataContext = viewModel;
            base.OnInitialized(e);
        }

        /// <summary>
        /// On application closing event
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }
        #endregion
    }
}
