﻿using System;
using System.Windows;
using System.Windows.Input;

namespace TakeABreak.ViewModels
{
    // <summary>
    /// Provides bindable properties and commands for the NotifyIcon
    /// </summary>
    public class NotifyIconViewModel
    {
        /// <summary>
        /// Shows the main window
        /// </summary>
        public ICommand ShowWindowCommand => new DelegateCommand
        {
            CanExecuteFunc = () => Application.Current.MainWindow != null && !Application.Current.MainWindow.IsVisible,
            CommandAction = () =>
            {
                Application.Current.MainWindow.Show();

                if (Application.Current.MainWindow.WindowState == WindowState.Minimized)
                {
                    Application.Current.MainWindow.WindowState = WindowState.Normal;
                }
            }
        };

        /// <summary>
        /// Hides the main window
        /// </summary>
        public ICommand HideWindowCommand => new DelegateCommand
        {
            CanExecuteFunc = () => Application.Current.MainWindow != null && Application.Current.MainWindow.IsVisible,
            CommandAction = () => Application.Current.MainWindow.Hide()
        };


        /// <summary>
        /// Shuts down the application
        /// </summary>
        public ICommand ExitApplicationCommand => new DelegateCommand
        {
            CanExecuteFunc = () => Application.Current.MainWindow != null,
            CommandAction = () => Application.Current.Shutdown()
        };
    }

    /// <summary>
    /// Simplistic delegate command.
    /// </summary>
    public class DelegateCommand : ICommand
    {
        public Action CommandAction { get; set; }
        public Func<bool> CanExecuteFunc { get; set; }

        public void Execute(object parameter)
        {
            CommandAction();
        }

        public bool CanExecute(object parameter)
        {
            return CanExecuteFunc == null || CanExecuteFunc();
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}
